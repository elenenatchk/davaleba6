package com.example.userprofileaplication
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.userprofileaplication.databinding.ActivityUpdateInfoBinding

class UpdateInfoActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUpdateInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        listener()
    }


    private fun listener() {
        binding.back.setOnClickListener {
            backInfo()
        }
        val fnup = intent.extras?.getString("fname", "")
        val ln = intent.extras?.getString("lname", "")
        val ag = intent.extras?.getInt("age", 0)
        val em = intent.extras?.getString("email", "")
        val gen = intent.extras?.getString("gender", "")
        binding.firstname.setText(fnup)
        binding.lastname.setText(ln)
        binding.ageupdate.setText(ag.toString())
        binding.genderupdate.setText(gen)
        binding.emailupdate.setText(em)

    }

    private fun backInfo() {
        val updatename = binding.firstname.text.toString()
        val updatelastname = binding.lastname.text.toString()
        val updateage = binding.ageupdate.text.toString()
        val updateemail = binding.emailupdate.text.toString()
        val genderupdate = binding.genderupdate.text.toString()


        val i = intent
        i.putExtra("fn", updatename)
        i.putExtra("ln", updatelastname)
        i.putExtra("ag", updateage)
        i.putExtra("em", updateemail)
        i.putExtra("gender", genderupdate)

        setResult(Activity.RESULT_OK, i)
        finish()

    }

}

