package com.example.userprofileaplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import com.example.userprofileaplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val REQUEST_CODE = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_Userprofileaplication)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        listener()
    }


    private fun listener() {


        binding.updatebtn.setOnClickListener {
            openUpdteActivity()

        }

    }


    private fun openUpdteActivity() {
        val intent = Intent(this, UpdateInfoActivity::class.java)
        intent.putExtra("fname", binding.fname.text.toString())
        intent.putExtra("lname", binding.lname.text.toString())
        intent.putExtra("age", binding.age.text.toString().toInt())
        intent.putExtra("email", binding.email.text.toString())
        intent.putExtra("gender", binding.gender.text.toString())
        startActivityForResult(intent, REQUEST_CODE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            val fn = data?.extras?.getString("fn", "").toString()
            val ln = data?.extras?.getString("ln", "").toString()
            val ag = data?.extras?.getString("ag", "").toString()
            val em = data?.extras?.getString("em", "").toString()
            val gen = data?.extras?.getString("gender", "").toString()



            binding.fname.text = fn
            binding.lname.text = ln
            binding.age.text = ag
            binding.email.text = em
            binding.gender.text = gen


        }


        super.onActivityResult(requestCode, resultCode, data)
    }


}



